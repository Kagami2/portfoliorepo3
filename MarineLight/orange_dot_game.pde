// radius of player
int radius = 15;
// the horizantel speed of the player (left/right speed)
int horiz = 5;
// the power of the jump
int jump = 7;
// x and y position of the player
float xp; 
float yp; 

// x and Y speed of the player;
float xs = 0; 
float ys = 0; 

// the resistance of movment, what makes you slow down to a stop
float r = 0.7; 
// the amount of gravity
float gravity = 0.4; 

// the booleans that controllw hich direction you are moving
boolean upPressed = false;
boolean downPressed = false;
boolean rightPressed = false;
boolean leftPressed = false;

// the numebr of enemys
int enemyNumber = 10;
// if you are alive
boolean alive = false;
// if you have won the game
boolean won = false;
// the text font
PFont font;
// the wave like movment variables for the enemys
float os;
float period = 300;
float amplitude = 100;
// the array that contains all the enemys
enemy[] enemys;
// a function that runs once at the beggiing of the program
void setup() {
  // size of the game window
  size(1440, 300);

  // sets the starting position of the player
  xp = width*0.2;
  yp = height/2;
  // create the enemy array
  enemys = new enemy[enemyNumber];

  // sets the rectmode to centered so when you draw a rectangle it bases the position off of the center of the rectangle, or else it would be the top left corner
  rectMode(CENTER);
  // same thing as rect mode, but for text
  textAlign(CENTER, CENTER);
  // takes of the stroke when you are drawing things
  noStroke();
  // imports the font form the name giving, you can find this font inside the data foloder of this sketch
  font = createFont("ADAM.otf", 100);
  // applys the font to the program
  textFont(font);
  // fills the enemys array withe enemys
  for (int i = 0; i<enemys.length; i++) {
    enemys[i] = new enemy(-16, 12, 6);
  }
}
// a function that runs and infinite number of times after void setup until you stop the program
void draw() {
  // stes the wave movment
  os = abs(amplitude * cos(TWO_PI * frameCount / period))*0.1;
  // gives the program a background color
  background(27);
  // if you are alive it runs the in game program
  if (alive) {
    // runs the function that lets you controll the player
    controll();
    // makes the player move
    move();
    // makes the walls solid
    walls();
    // displays the player
    display();
    // runs all the enemys
    for (int i = 0; i<enemys.length; i++) {
      // goes inside the specific enemy and finds the "run" function, then runs it
      enemys[i].run();
    }
    // if you are dead though:
  } else if (!alive) {
    // and you have not won
    if (!won) {
      // then make the text white
      fill(255);
      // and write play again
      text("play again", width/2, height/2);
      // if you are dead, but you have won
    } else if (won) {
      // then make the text green
      fill(0, 255, 0);
      // and write:
      //YOU WON
      //PLAY AGAIN
      text("YOU WON \n PLAY AGAIN", width/2, height/2);
    }
  }
}
//  a function that makes you be able to controll the player
void controll() {
  // if the right key is pressed then make the x-speed be going to the right
  if (rightPressed) {
    xs = horiz;
  }
  // if the left key is pressed then make the x-speed be going left
  if (leftPressed) {
    xs = -horiz;
  }
  // if the up key is pressed then make the y-speed be going up
  if (upPressed) {
    ys = -jump;
  }
  // if the down key is pressed then make the y-speed be going down
  if (downPressed) {
    ys += jump/2;
  }
}
// makes the player actually move
void move() {
  // adds the x speed to the x position
  xp += xs;
  //and the y speed to the y positions
  yp += ys;

  // adds grvaity to the y speed
  ys += gravity;
  // adds resistance to the x speed
  xs *= r;
}
// draws the player
void display() {
  // sets the fill color to white
  fill(255);
  // draws a rectangle at the players position with his radius
  rect(xp, yp, radius*2, radius*2);
}
// makes the walls solid
void walls() {
  // top wall collision
  if (yp <= radius) {
    yp = radius+1;
    ys = -ys*(r/2);
  }

  // top wall collision
  if (yp >= height-radius) {
    yp = height-(radius+1);
    //ys = -ys*(r/2);
  }


  // left wall collision
  if (xp <= radius) {
    xp = radius+1;
    xs = -xs*(r/2);
  }
  // this does not make the  right wall solid, but it makes you win and die when you pass the right wall
  if (xp >= width+radius) {
    alive = false;
    won = true;
  }
}
// sets the key booleans to there corresponding values
void keyPressed() {
  // if the up key is being pressed then make the upPressed be true
  if (keyCode == UP) {
    upPressed = true;
  }
  // if the down key is being pressed then make the downPressed be true
  if (keyCode == DOWN) {
    downPressed = true;
  }
  // if the left key is being pressed then make the leftPressed be true
  if (keyCode == LEFT) {
    leftPressed = true;
  }
  // if the right key is being pressed then make the rightPressed be true
  if (keyCode == RIGHT) {
    rightPressed = true;
  }
}
// makes the key booleans go back to false when there keys have been released
void keyReleased() {
  // when the up key has been released, then the upPressed is turned back to false
  if (keyCode == UP) {
    upPressed = false;
  }
  // when the down key has been released, then the downPressed is turned back to false
  if (keyCode == DOWN) {
    downPressed = false;
  }
  // when the left key has been released, then the leftPressed is turned back to false
  if (keyCode == LEFT) {
    leftPressed = false;
  }
  // when the right key has been released, then the rightPressed is turned back to false
  if (keyCode == RIGHT) {
    rightPressed = false;
  }
}
// a function that runs when ever the mouse is released
void mouseReleased() {
  // if the mouse is released and you are dead
  if (!alive) {
    // then come back to life
    alive = true;
    // make sure you havent won
    won = false;
    // respawn all the enemys
    for (int i = 0; i<enemys.length; i++) {
      enemys[i].respawn();
    }
    
    // reset your speed
    xs = 0; 
    ys = 0;
    
    // and reset your position
    xp = width*0.2;
    yp = height/2;
  }
}